part of 'images_bloc.dart';

abstract class ImagesEvent extends Equatable {
  const ImagesEvent();
}

class ImagesLoadingStart extends ImagesEvent {
  @override
  List<Object> get props => [];
}

class ImagesRefreshRequested extends ImagesEvent {
  @override
  List<Object> get props => [];
}
