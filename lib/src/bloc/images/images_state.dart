part of 'images_bloc.dart';

abstract class ImagesState extends Equatable {
  const ImagesState();
}

class ImagesInitial extends ImagesState {
  @override
  List<Object> get props => [];
}

class ImagesLoading extends ImagesState {
  @override
  List<Object> get props => [];
}

class ImagesLoaded extends ImagesState {
  final List<ImageModel> images;

  const ImagesLoaded({
    @required this.images,
  });

  @override
  List<Object> get props => [images];
}

class ImagesError extends ImagesState {
  final String errorText;

  const ImagesError({
    @required this.errorText,
  });

  @override
  List<Object> get props => [];
}
