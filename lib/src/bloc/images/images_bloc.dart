import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:test_devstrean/src/models/image_model.dart';
import 'package:test_devstrean/src/repositories/images_repository.dart';

part 'images_event.dart';
part 'images_state.dart';

class ImagesBloc extends Bloc<ImagesEvent, ImagesState> {
  final ImagesRepository imagesRepository;
  ImagesBloc({@required this.imagesRepository})
      : assert(imagesRepository != null),
        super(ImagesInitial());

  @override
  Stream<ImagesState> mapEventToState(
    ImagesEvent event,
  ) async* {
    if (event is ImagesLoadingStart) {
      yield* _mapImagesLoadingStartToState(event);
    }

    if (event is ImagesRefreshRequested) {
      yield* _mapImagesRefreshRequested(event);
    }
  }

  Stream<ImagesState> _mapImagesRefreshRequested(
      ImagesRefreshRequested event) async* {
    try {
      var images = await imagesRepository.getImages();
      yield ImagesLoaded(images: images);
    } catch (_) {
      print('error');
    }
  }

  Stream<ImagesState> _mapImagesLoadingStartToState(
    ImagesLoadingStart event,
  ) async* {
    yield (ImagesLoading());
    try {
      var images = await imagesRepository.getImages();
      yield ImagesLoaded(images: images);
    } catch (error) {
      yield ImagesError(errorText: error.toString());
    }
  }
}
