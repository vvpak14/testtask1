import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:test_devstrean/src/consts.dart';
import 'package:test_devstrean/src/models/image_model.dart';

class ImagesRepository {
  Future<List<ImageModel>> getImages() async {
    var response = await http.get('$kURL/photos?client_id=$kClientId');

    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);
      var images = List<ImageModel>();
      jsonResponse.forEach((image) {
        images.add(ImageModel.fromJson(image));
      });
      return images;
    }

    //TODO: change this
    throw new Exception('Something went wrong');
  }
}
