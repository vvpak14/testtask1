import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_devstrean/src/bloc/images/images_bloc.dart';
import 'package:test_devstrean/src/consts.dart';
import 'package:test_devstrean/src/models/image_model.dart';
import 'package:test_devstrean/src/models/image_screen_arguments.dart';
import 'package:test_devstrean/src/widgets/spinner.dart';

import 'image_screen.dart';

class ImagesScreen extends StatefulWidget {
  static final routeName = '/images';

  @override
  _ImagesScreenState createState() => _ImagesScreenState();
}

class _ImagesScreenState extends State<ImagesScreen> {
  Completer<void> _refreshCompleter;

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Unsplash  app'),
      ),
      body: BlocConsumer<ImagesBloc, ImagesState>(
        listener: (context, state) {
          if (state is ImagesLoaded) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
          }
        },
        builder: (context, state) {
          if (state is ImagesLoading) {
            return Spinner();
          }
          if (state is ImagesLoaded) {
            return Container(
              child: RefreshIndicator(
                onRefresh: () {
                  BlocProvider.of<ImagesBloc>(context).add(
                    ImagesRefreshRequested(),
                  );
                  return _refreshCompleter.future;
                },
                child: ListView.builder(
                  itemCount: state.images.length,
                  itemBuilder: (context, index) {
                    var image = state.images.elementAt(index);
                    return ImageListElement(image: image);
                  },
                ),
              ),
            );
          }

          if (state is ImagesError) {
            return Container(
              child: Text(state.errorText),
            );
          }

          return Container(
            child: Center(
              child: Text('Unknown error'),
            ),
          );
        },
      ),
    );
  }
}

class ImageListElement extends StatelessWidget {
  const ImageListElement({
    Key key,
    @required this.image,
  }) : super(key: key);

  final ImageModel image;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          ImageScreen.routeName,
          arguments: ImageScreenArguments(
            url: image.urls.full,
            id: image.id,
          ),
        );
      },
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Hero(
              tag: 'imageHero${image.id}',
              child: CachedNetworkImage(
                placeholder: (context, url) => CircularProgressIndicator(),
                imageUrl: image.urls.full,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              image.altDescription,
              style: kDescriptionTextStyle,
            ),
            Text(
              image.user.username,
              style: kAuthorTextStyle,
            ),
          ],
        ),
      ),
    );
  }
}
