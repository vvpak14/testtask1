import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:test_devstrean/src/models/image_screen_arguments.dart';

class ImageScreen extends StatelessWidget {
  static final routeName = '/image';
  @override
  Widget build(BuildContext context) {
    final ImageScreenArguments arguments =
        ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text('Picture'),
      ),
      body: Container(
        child: Center(
          child: InteractiveViewer(
            boundaryMargin: EdgeInsets.all(20.0),
            minScale: 0.1,
            maxScale: 2.6,
            child: Hero(
              tag: 'imageHero${arguments.id}',
              child: CachedNetworkImage(
                placeholder: (context, url) => CircularProgressIndicator(),
                imageUrl: arguments.url,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
