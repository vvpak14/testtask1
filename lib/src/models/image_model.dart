import 'package:flutter/foundation.dart';
import 'package:test_devstrean/src/models/links.dart';
import 'package:test_devstrean/src/models/sponsor.dart';
import 'package:test_devstrean/src/models/sponsorhip.dart';
import 'package:test_devstrean/src/models/urls.dart';

class ImageModel {
  String id;
  String createdAt;
  String updatedAt;
  DateTime promotedAt;
  int width;
  int height;
  String color;
  String description;
  String altDescription;
  Urls urls;
  Links links;
  List<Category> categories;
  int likes;
  bool likedByUser;
  List<String> currentUserCollections;
  Sponsorship sponsorship;
  Sponsor user;

  ImageModel(
      {this.id,
      this.createdAt,
      this.updatedAt,
      this.promotedAt,
      this.width,
      this.height,
      this.color,
      this.description,
      this.altDescription,
      this.urls,
      this.links,
      this.categories,
      this.likes,
      this.likedByUser,
      this.currentUserCollections,
      this.sponsorship,
      this.user});

  ImageModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    promotedAt = json['promoted_at'] != null
        ? DateTime.parse(json['promoted_at'])
        : null;
    width = json['width'];
    height = json['height'];
    color = json['color'];
    description = json['description'];
    altDescription = json['alt_description'];
    urls = json['urls'] != null ? new Urls.fromJson(json['urls']) : null;
    links = json['links'] != null ? new Links.fromJson(json['links']) : null;
    if (json['categories'] != null) {
      categories = new List<Null>();
      json['categories'].forEach((v) {
        //TODO: fix this
//        categories.add(new Null.fromJson(v));
      });
    }
    likes = json['likes'];
    likedByUser = json['liked_by_user'];
    if (json['current_user_collections'] != null) {
      currentUserCollections = new List<Null>();
      json['current_user_collections'].forEach((v) {
        //TODO: fix this
//        currentUserCollections.add(new Null.fromJson(v));
      });
    }
    sponsorship = json['sponsorship'] != null
        ? new Sponsorship.fromJson(json['sponsorship'])
        : null;
    user = json['user'] != null ? new Sponsor.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['promoted_at'] = this.promotedAt;
    data['width'] = this.width;
    data['height'] = this.height;
    data['color'] = this.color;
    data['description'] = this.description;
    data['alt_description'] = this.altDescription;
    if (this.urls != null) {
      data['urls'] = this.urls.toJson();
    }
    if (this.links != null) {
      data['links'] = this.links.toJson();
    }
    if (this.categories != null) {
      //TODO: fix this
//      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    data['likes'] = this.likes;
    data['liked_by_user'] = this.likedByUser;
    if (this.currentUserCollections != null) {
      //TODO: fix this

//      data['current_user_collections'] =
//          this.currentUserCollections.map((v) => v.toJson()).toList();
    }
    if (this.sponsorship != null) {
      data['sponsorship'] = this.sponsorship.toJson();
    }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}
