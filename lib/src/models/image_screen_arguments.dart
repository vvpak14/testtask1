import 'package:flutter/material.dart';

class ImageScreenArguments {
  final String url;
  final String id;

  ImageScreenArguments({
    @required this.url,
    @required this.id,
  });
}
