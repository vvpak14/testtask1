import 'package:test_devstrean/src/models/sponsor.dart';

class Sponsorship {
  List<String> impressionUrls;
  String tagline;
  String taglineUrl;
  Sponsor sponsor;

  Sponsorship(
      {this.impressionUrls, this.tagline, this.taglineUrl, this.sponsor});

  Sponsorship.fromJson(Map<String, dynamic> json) {
    if (json['impression_urls'] != null) {
      impressionUrls = new List<Null>();
//      json['impression_urls'].forEach((v) {
//        impressionUrls.add(new Null.fromJson(v));
//      });
    }
    tagline = json['tagline'];
    taglineUrl = json['tagline_url'];
    sponsor =
        json['sponsor'] != null ? new Sponsor.fromJson(json['sponsor']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.impressionUrls != null) {
//      data['impression_urls'] =
//          this.impressionUrls.map((v) => v.toJson()).toList();
    }
    data['tagline'] = this.tagline;
    data['tagline_url'] = this.taglineUrl;
    if (this.sponsor != null) {
      data['sponsor'] = this.sponsor.toJson();
    }
    return data;
  }
}
