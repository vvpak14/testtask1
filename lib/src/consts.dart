import 'package:flutter/cupertino.dart';

const kURL = 'https://api.unsplash.com';

const kClientId =
    'ab3411e4ac868c2646c0ed488dfd919ef612b04c264f3374c97fff98ed253dc9';

const kDescriptionTextStyle = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.bold,
);
const kAuthorTextStyle = TextStyle(
  fontSize: 14,
  fontStyle: FontStyle.italic,
);
