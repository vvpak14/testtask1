import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_devstrean/src/bloc/images/images_bloc.dart';
import 'package:test_devstrean/src/repositories/images_repository.dart';
import 'package:test_devstrean/src/screens/image_screen.dart';
import 'package:test_devstrean/src/screens/images_screen.dart';

class PrintBlocObserver extends BlocObserver {
  @override
  void onChange(Cubit cubit, Change change) {
    print('${cubit.runtimeType} $change');
    super.onChange(cubit, change);
  }
}

void main() {
  Bloc.observer = PrintBlocObserver();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final ImagesRepository imagesRepository = ImagesRepository();

  @override
  Widget build(BuildContext context) {
    //Bloc provider should be placed lower in the tree
    return BlocProvider(
      create: (context) => ImagesBloc(
        imagesRepository: imagesRepository,
      )..add(ImagesLoadingStart()),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        initialRoute: ImagesScreen.routeName,
        routes: {
          ImagesScreen.routeName: (context) => ImagesScreen(),
          ImageScreen.routeName: (context) => ImageScreen(),
        },
      ),
    );
  }
}
