# Используемые фишки

- Printing to the console +
- assigning variables +
- Checking for null or zero +
- Asynchronous programming +
- Futures +
- async, await +
- widgets (Layout widgets, ListView) +
- TextStyle +
- Material library +
- widget tree +
- reuse of a widget +
- assets +
- load images over a network +
- ThemeData class -
- StatefulWidget +
- StatelessWidget +
- constructor +
- shared_preferences plugin - (По условиям задачи не обязательно)
- Route, Navigator +
- TabController, TabBar, Tab, TabBarView - (По условиям задачи не обязательно)
- Drawer - (По условиям задачи не обязательно)
- PanResponder - (По условиям задачи не обязательно, использовал GestureDetector)
- http package +
- Animation, AnimationController, FadeTransition, Dismissible +- (Hero animation)

# Примечание

Дизайн и структура кода делалась на скорую руку. Можно сделать лучше.